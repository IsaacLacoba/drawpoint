package com.isaac.fail.drawpoint;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.view.MotionEvent;
import android.view.View;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by isaac on 30/03/16.
 */
public class PaintView extends View {
    private static final String TAG = "PaintView";
    private static final int OFFSET = 50;

    private ArrayList<Paint> paintArrayList;

    private ArrayList<Path> mDrawingPaths;
    private Map<Integer, MotionEventCallback> actionHooks = new HashMap();
    private Map<Integer, PaintCallback> paintHooks = new HashMap();

    private static final float THIN_WIDTH = 1.5f;
    private static final float BOLD_WIDTH = 10.f;
    private float mPointSize;
    private float mX, mY;
    private float startX, startY, endX, endY;

    private int mPaintMode = 0;

    private Bitmap pointer;


    public PaintView(Context context) {
        super(context);
        pointer = BitmapFactory.decodeResource(getResources(), R.drawable.pencil);
        mPointSize = THIN_WIDTH;

        initDrawingArrayLists();
        registerPaintCallbacks();
        registerMotionActions();
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        pointer.recycle();
    }


    public void setPaintMode(int mode) {
        mPaintMode = mode;
        mPointSize = mode == PaintModes.LINE_MODE? THIN_WIDTH: BOLD_WIDTH;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (mPaintMode == PaintModes.CLEAN_MODE) {
            paintHooks.get(PaintModes.CLEAN_MODE).draw(canvas);
            return;
        }

        drawPaths(canvas);
    }


    private void drawPaths(Canvas canvas) {
        for(Integer hookIndex: paintHooks.keySet()) {
            if(hookIndex == PaintModes.CLEAN_MODE)
                break;
            paintHooks.get(hookIndex).draw(canvas);
        }
    }

    private void registerMotionActions() {
        addActionHook(MotionEvent.ACTION_DOWN, new MotionEventCallback() {
            @Override
            public void execute(float x, float y) {
                startX = x;
                startY = y;
                mDrawingPaths.get(mPaintMode).moveTo(x, y);
                mDrawingPaths.get(mPaintMode).addCircle(x, y, mPointSize / 4, Path.Direction.CW);
            }
        });

        addActionHook(MotionEvent.ACTION_MOVE, new MotionEventCallback() {
            @Override
            public void execute(float x, float y) {
                mDrawingPaths.get(mPaintMode).lineTo(x, y);
            }
        });

        addActionHook(MotionEvent.ACTION_UP, new MotionEventCallback() {
            @Override
            public void execute(float x, float y) {
                endX = x;
                endY = y;
            }
        });
    }

    private void registerPaintCallbacks() {
        addPaintHook(PaintModes.LINE_MODE, new PaintCallback() {
            @Override
            public void draw(Canvas canvas) {
                canvas.drawBitmap(pointer, mX, mY - OFFSET, paintArrayList.get(mPaintMode));
                canvas.drawPath(mDrawingPaths.get(PaintModes.LINE_MODE), paintArrayList.get(PaintModes.LINE_MODE));
            }
        });

        addPaintHook(PaintModes.POINT_MODE, new PaintCallback() {
            @Override
            public void draw(Canvas canvas) {
                canvas.drawBitmap(pointer, mX, mY - OFFSET, paintArrayList.get(mPaintMode));
                canvas.drawPath(mDrawingPaths.get(PaintModes.POINT_MODE), paintArrayList.get(PaintModes.POINT_MODE));

            }
        });

        addPaintHook(PaintModes.CLEAN_MODE, new PaintCallback() {
            @Override
            public void draw(Canvas canvas) {
                for (Path path : mDrawingPaths)
                    path.reset();
                canvas.drawColor(Color.WHITE);
                setPaintMode(PaintModes.LINE_MODE);
                canvas.drawBitmap(pointer, mX, mY - OFFSET, paintArrayList.get(mPaintMode));
            }
        });
    }

    private void initDrawingArrayLists() {
        paintArrayList = new ArrayList<Paint>();
        mDrawingPaths = new ArrayList<Path>();

        paintArrayList.add(PaintFactory.createLinePaint(BOLD_WIDTH));
        mDrawingPaths.add(new Path());

        paintArrayList.add(PaintFactory.createLinePaint(THIN_WIDTH));
        mDrawingPaths.add(new Path());

    }

    private void addActionHook(Integer key, MotionEventCallback callback) {
        actionHooks.put(key, callback);
    }

    private void addPaintHook(Integer key, PaintCallback callback) {
        paintHooks.put(key, callback);
    }


    public boolean onTouchEvent(MotionEvent event) {
        invalidate();

        mX = event.getX();
        mY = event.getY();

        if (actionHooks.containsKey(event.getAction())) {
            actionHooks.get(event.getAction()).execute(event.getX(), event.getY());
            return true;
        }

        return false;
    }
}


