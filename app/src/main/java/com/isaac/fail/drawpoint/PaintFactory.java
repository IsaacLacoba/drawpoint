package com.isaac.fail.drawpoint;

import android.graphics.Color;
import android.graphics.Paint;

/**
 * Created by isaac on 1/04/16.
 */
public class PaintFactory {
    public PaintFactory() {

    }

    public static Paint createLinePaint(float width) {
        Paint paint = new Paint();
        paint.setColor(Color.BLACK);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(width);
        paint.setAntiAlias(true);
        paint.setStrokeCap(Paint.Cap.SQUARE);
        return paint;
    }
}
