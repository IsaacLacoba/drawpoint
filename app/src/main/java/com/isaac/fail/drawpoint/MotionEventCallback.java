package com.isaac.fail.drawpoint;

/**
 * Created by isaac on 30/03/16.
 */
public interface MotionEventCallback {
    public void execute(float x, float y);
}
