package com.isaac.fail.drawpoint;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;

public class MainActivity extends AppCompatActivity {
    private LinearLayout layout;
    private PaintView paintView;
    private ImageButton lineButton, pointButton, cleanButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        layout= (LinearLayout) findViewById(R.id.layout);
        paintView = new PaintView(MainActivity.this);
        layout.addView(paintView);

        lineButton = (ImageButton) findViewById(R.id.lineButton);
        lineButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                paintView.setPaintMode(PaintModes.LINE_MODE);
            }
        });
        pointButton= (ImageButton) findViewById(R.id.pointButton);
        pointButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                paintView.setPaintMode(PaintModes.POINT_MODE);
            }
        });

        cleanButton = (ImageButton) findViewById(R.id.cleanButton);
        cleanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                paintView.setPaintMode(PaintModes.CLEAN_MODE);
                paintView.invalidate();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.about) {
            showAboutInfo();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void showAboutInfo() {
        Snackbar.make(findViewById(R.id.layout), getResources().getString(R.string.about_author), Snackbar.LENGTH_LONG)
                .show();
    }
}

